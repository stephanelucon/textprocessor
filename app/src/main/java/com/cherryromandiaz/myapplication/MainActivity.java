package com.cherryromandiaz.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cherryromandiaz.myapplication.util.backend.Constants;
import com.cherryromandiaz.myapplication.util.backend.Endpoint;
import com.cherryromandiaz.myapplication.util.storage.RunTimeStorage;
import com.cherryromandiaz.myapplication.util.storage.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final boolean translationMode = true;
    String modelInText;
    String firstText;
    String modelOutText;
    String secondText;
    String requestText;
    String n;
    String tokens;
    String topProb;
    String pres;
    String freq;
    String lProb;
    String stop;
    String bearertoken;
    String submittedFocus = "";
    private boolean runningDropDownRoutine = false;
    private Spinner spinnerResults;
    private RadioGroup  radioGroup;
    private Button buttonResults;
    private Button buttonPutEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferencesManager.initializePreferences(MainActivity.this);

        /*request button*/
        Button button = findViewById(R.id.button);

        /** model fields */
        final EditText modelIn = findViewById(R.id.modelIn);
        modelIn.setText(SharedPreferencesManager.getModelIn());
        final EditText modelOut = findViewById(R.id.modelOut);
        modelOut.setText(SharedPreferencesManager.getModelOut());

        /*first edit text*/
        final EditText editText = findViewById(R.id.fname);
        editText.setText(SharedPreferencesManager.getOrigin());

        /*second edit text*/
        final EditText editText2 = findViewById(R.id.fname2);
        editText2.setText(SharedPreferencesManager.getDestination());

        final EditText choice = findViewById(R.id.editTextChoice);
        choice.setText("5");

        final EditText maxToken = findViewById(R.id.editTextMaxToken);
        maxToken.setText("200");

        final EditText topP = findViewById(R.id.editTextTopP);
        topP.setText("0.5");

        final EditText presence = findViewById(R.id.editTextPresence);
        presence.setText("1");

        final EditText frequency = findViewById(R.id.editTextFrequency);
        frequency.setText("0.6");

        final EditText logProb = findViewById(R.id.editTextLogProb);
        logProb.setText("10");

        final EditText sentences = findViewById(R.id.editTextSentences);
        sentences.setText("5");

        final EditText stopText = findViewById(R.id.editTextStop);
        stopText.setText(".");

        final EditText bearertokenText = findViewById(R.id.editTextBearer);
        bearertokenText.setText("");

        final TextView submittedText = findViewById(R.id.submitted);


        spinnerResults = findViewById(R.id.spinner);
        buttonResults = findViewById(R.id.button2);
        buttonPutEdit = findViewById(R.id.button3);
        /* radio buttons holder */
        radioGroup = findViewById(R.id.radioGroup);

        /* adding spinner to text2 */
        buttonPutEdit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                try {
                    String selectedtext = getRadioGroupSelected();

                    editText2.setText(editText2.getText() + selectedtext + ".");

                    /* put cursor approximately where we're working */
                    int position = editText2.getText().length();
                    if(editText.getText().length()<position)
                        editText.setSelection(editText.getText().length());
                    else
                        editText.setSelection(position);
                    editText2.setSelection(position);
                } catch (Exception e) {
                    Log.d("EXCEPTION", e.toString());
                }

            }
        });

        /*adding spinner to text2 + requesting new completion*/
        buttonResults.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                try {
                    String selectedtext = getRadioGroupSelected();

                    editText2.setText(editText2.getText() + selectedtext + ".");
                } catch (Exception e) {
                    Log.d("EXCEPTION", e.toString());
                }

                /** and let's always ask for a request */
                RunTimeStorage.getInstace().setTextArray(null);

                /* modelInText from view */
                modelInText = String.valueOf(modelIn.getText());
                SharedPreferencesManager.setModelIn(modelInText);

                /* right side model */
                modelOutText = String.valueOf(modelOut.getText());
                SharedPreferencesManager.setModelOut(modelOutText);

                /* fname from view */
                firstText = String.valueOf(editText.getText());
                SharedPreferencesManager.setOrigin(firstText);

                secondText = String.valueOf(editText2.getText());
                SharedPreferencesManager.setDestination(secondText);

                /* put cursor approximately where we're working */
                int position = editText2.getText().length();
                if(editText.getText().length()<position)
                    editText.setSelection(editText.getText().length());
                else
                    editText.setSelection(position);
                editText2.setSelection(position);

                Log.d("body prep", modelInText + firstText + modelOutText);
                //TODO: fix the next 3 lines so that we get last 3 sentences from the initial text, without crash...

                int s = 3;

                try {
                    s = Integer.parseInt(sentences.getText().toString());
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }

                modelInText = getLastXphrases(getLastXphrases(modelInText, s, true), s, true);

                modelOutText = getLastXphrases(getLastXphrases(modelOutText, s, true), s, true);

                int outputSize = getSizeOfArray(secondText);
                int back = Integer.parseInt(sentences.getText().toString());

                /** TRANSLATION MODE
                 * now we want to ask only meaningfull sentence according to where we are in the process */
                if (translationMode) {

                    Log.d("body size for pick", String.valueOf(outputSize));
                    firstText = getNextThreePhrases(firstText, outputSize, back);
                    Toast.makeText(MainActivity.this, firstText, Toast.LENGTH_LONG);
                }
                Log.d("body prep firstText", firstText);

                String submittedTextView = firstText;
                submittedTextView = submittedTextView.replaceAll("\n", " ");
                submittedTextView = "Focussed (defined from an existing output of " + outputSize + " and search for the last " + back + " sentences)\n" + submittedTextView;
                submittedText.setText(submittedTextView);

                secondText = getLastXphrases(getLastXphrases(secondText, s - 1, false), s - 1, false);
                Log.d("body prep secontext", secondText);

                Log.d("body prep get1st3", modelInText + firstText + modelOutText + secondText);


                requestText = modelInText + firstText + modelOutText + secondText;
                /*if (!secondText.equals("")) {
                    requestText = requestText + "\n" + secondText;
                }*/
                n = String.valueOf(choice.getText());
                tokens = String.valueOf(maxToken.getText());
                topProb = String.valueOf(topP.getText());
                pres = String.valueOf(presence.getText());
                freq = String.valueOf(frequency.getText());
                lProb = String.valueOf(logProb.getText());
                stop = String.valueOf(stopText.getText());
                bearertoken =String.valueOf(bearertokenText.getText());

                makeRequest(requestText, n, tokens, topProb, pres, freq, lProb, stop);

                startDropDownRoutine();
                //setResultVisibility(false);

            }
        });

        /* Asking for a series of completions */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RunTimeStorage.getInstace().setTextArray(null);

                /* modelInText from view */
                modelInText = String.valueOf(modelIn.getText());
                SharedPreferencesManager.setModelIn(modelInText);

                /* right side model */
                modelOutText = String.valueOf(modelOut.getText());
                SharedPreferencesManager.setModelOut(modelOutText);

                /* fname from view */
                firstText = String.valueOf(editText.getText());
                SharedPreferencesManager.setOrigin(firstText);

                secondText = String.valueOf(editText2.getText());
                SharedPreferencesManager.setDestination(secondText);
                int position = editText2.getText().length();
                if(editText.getText().length()<position)
                    editText.setSelection(editText.getText().length());
else
                editText.setSelection(position);

                editText2.setSelection(position);

                Log.d("body prep", modelInText + firstText + modelOutText);
                //TODO: fix the next 3 lines so that we get last 3 sentences from the initial text, without crash...
                int s = 3;

                try {
                    s = Integer.parseInt(sentences.getText().toString());
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }

                modelInText = getLastXphrases(getLastXphrases(modelInText, s, true), s, true);

                modelOutText = getLastXphrases(getLastXphrases(modelOutText, s, true), s, true);

                int outputSize = getSizeOfArray(secondText);
                int back = Integer.parseInt(sentences.getText().toString());

                /** TRANSLATION MODE
                 * now we want to ask only meaningfull sentence according to where we are in the process */
                if (translationMode) {

                    Log.d("body size for pick", String.valueOf(outputSize));
                    firstText = getNextThreePhrases(firstText, outputSize, back);
                    Toast.makeText(MainActivity.this, firstText, Toast.LENGTH_LONG);

                }
                Log.d("body prep firstText", firstText);

                String submittedTextView = firstText;
                submittedTextView = submittedTextView.replaceAll("\n", " ");
                submittedTextView = "Focussed (defined from an existing output of " + outputSize + " and search for the last " + back + " sentences)\n" + submittedTextView;
                submittedText.setText(submittedTextView);

                secondText = getLastXphrases(getLastXphrases(secondText, s - 1, false), s - 1, false);
                Log.d("body prep secontext", secondText);

                Log.d("body prep get1st3", modelInText + firstText + modelOutText + secondText);


                requestText = modelInText + firstText + modelOutText + secondText;
                /*if (!secondText.equals("")) {
                    requestText = requestText + "\n" + secondText;
                }*/
                n = String.valueOf(choice.getText());
                tokens = String.valueOf(maxToken.getText());
                topProb = String.valueOf(topP.getText());
                pres = String.valueOf(presence.getText());
                freq = String.valueOf(frequency.getText());
                lProb = String.valueOf(logProb.getText());
                stop = String.valueOf(stopText.getText());
                bearertoken = String.valueOf(bearertokenText.getText());

                makeRequest(requestText, n, tokens, topProb, pres, freq, lProb, stop);
                Toast.makeText(MainActivity.this, requestText, Toast.LENGTH_LONG);
                startDropDownRoutine();
            }
        });
    }

    private String getRadioGroupSelected() {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        int idx = radioGroup.indexOfChild(radioButton);

        RadioButton r = (RadioButton) radioGroup.getChildAt(idx);

        return r.getText().toString();
    }

    private String getLastXphrases(String text, Integer i, boolean addLastPoint) {

        /*return if has only one character*/
        if (text.length() < 2) {
            return "";
        }

        boolean hadPoint = false;
        if (text.length() > 0) if (text.endsWith(".")) hadPoint = true;

        /*add a dot if the text has none*/
        if (!text.contains(".")) {
            text = text + ".";
        }

        /* Split my array */
        String[] array = text.split("\\.");

        /* Create a list to work with */
        ArrayList<String> arrayList = new ArrayList<String>();
        /* Add all split string to an array */
        Collections.addAll(arrayList, array);
        /* Reverse my list */
        Collections.reverse(arrayList);

        StringBuilder returnString = new StringBuilder();

        /* Check if we have sentences if not use length */
        for (int j = 0; j < (arrayList.size() < i ? arrayList.size() : i); j++) {
            returnString.append(arrayList.get(j));
            if (j < (arrayList.size() < i - 1 ? arrayList.size() : i - 1))
                returnString.append(".");
            else if (hadPoint || addLastPoint)
                returnString.append(".");

        }

        return returnString.toString();
    }


    private String getNextThreePhrases(String text, Integer k, Integer back) {

        /* handling cas when we don't gave 2 sentences in output text*/
        if (k < 2) k = 2;

        /*return if has only one character*/
        if (text.length() < 2) {
            return "";
        }
        /*add a dot if the text has none*/
        if (!text.contains(".")) {
            text = text + ".";
        }
        Log.d("before for", "preparing for return string:" + text);
        /* Split my array */
        String[] array = text.split("\\.");

        /* Create a list to work with */
        ArrayList<String> arrayList = new ArrayList<String>();
        /* Add all split string to an array */
        Collections.addAll(arrayList, array);

        StringBuilder returnString = new StringBuilder();

        Log.d("before for", "k = " + k);

        int startPoint = 0;
        /* say we have an output of N elements
         * but we want to focuss only on the last X elements
         * from a list of Z elements
         * we start at Z-X unless Z<X, then we start at 0
         *  we want to get te last N elements
         * */
        if (arrayList.size() > back) {
            if (k - back > 0)
                startPoint = k - back - 1;
            else
                startPoint = 0;
        }

        int endPoint = arrayList.size() - 1;
        /* here is where we stop.
         *  we need to get only one more element compared to the existing output */
        if (arrayList.size() > k) endPoint = k;


        Log.d("FOR", "rightOutputSize =" + k + " arrayListSize =" + arrayList.size() + " startpoint =" + startPoint + " to endpoint =" + endPoint);
        /* Check if we have sentences if not use length */
        for (int j = startPoint; j <= (arrayList.size() > k ? k : arrayList.size()); j++) {
/*        for ( int j = (k-back <1 ? (arrayList.size()>=k ? (arrayList.size()-k):0) : (arrayList.size()>=back ? (arrayList.size()-back):0)); j <= (arrayList.size() > k ? k : arrayList.size()); j++) {
 last working version :       for ( int j = (k-back <1 ? k-2 : k-back); j <= (arrayList.size() > k ? k : arrayList.size()-1); j++) {
 */
            /*  for ( int j = 0; j==4; j++) {*/

            try {
                returnString.append(arrayList.get(j));
            } catch (Exception e) {
                returnString.append(arrayList.get(0));

            }

            returnString.append(".");

            Log.d("for ", "j=" + j + " from arrayList size of " + arrayList.size() + " of a k =" + k + " and returnString is " + returnString.toString());

        }
        Log.d("body returnString ", returnString.toString());
        Toast.makeText(MainActivity.this, returnString.toString(), Toast.LENGTH_LONG);
        return returnString.toString();
    }


    private int getSizeOfArray(String text) {
        if (!text.contains(".")) {
            text = text + ".";
        }
        String[] array = text.split("\\.");

        /* Create a list to work with */
        ArrayList<String> arrayList = new ArrayList<String>();
        /* Add all split string to an array */
        Collections.addAll(arrayList, array);

        int size = arrayList.size();
        return size;

    }

    private void logJSONExample() {

        /** this method was useful to construct an in:/out: prompt but seem less useful if the editText1 + editText2 approach is working */
        String text = "Suntem pe o strada aglomerata, in Lipscan.\n\nLIVIU. \nBai nu pot sa cred. Bla bla, ceva de spus, altceva si inca ceva, si apoi asta a fost bla bla";


        text = text.replaceAll("\\.\\.\\.", "∏#");
        text = text.replaceAll("[.]", ".# ");
        text = text.replaceAll("[!]", "!#");
        text = text.replaceAll("[?]", "?#");
        text = text.replaceAll("[;]", ";#");
        text = text.replaceAll("[:]", ":#");
        text = text.replaceAll("[∏]", "...#");
        Log.d("TAG", text);

        String[] parts = text.split("[\\#]");
//        String[] parts = text.split("[.]");

        Log.d("TAG", String.valueOf(parts.length));

        List<JSONObject> list = new ArrayList<JSONObject>();

        for (int i = 0; i < parts.length; i++) {
            String name = parts[i];

            JSONObject object = new JSONObject();

            try {
                object.put("id", i);
                object.put("text", name);

                list.add(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", parts[i]);

        }
    }

    public void startDropDownRoutine() {
        if (!runningDropDownRoutine) {
            runningDropDownRoutine = true;
            new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    if (RunTimeStorage.getInstace().getTextArray() != null) {
                        setResultVisibility(RunTimeStorage.getInstace().getTextArray().length() > 0);
                    } else {
                        this.start();

                    }
                }
            }.start();
        }
    }

    private void setResultVisibility(Boolean status) {
        if (status) {
            spinnerResults.setVisibility(View.VISIBLE);
            buttonResults.setVisibility(View.VISIBLE);

//            String[] array = new String[RunTimeStorage.getInstace().getTextArray().length()];
//            JSONArray choices = RunTimeStorage.getInstace().getTextArray();
//            for (int i = 0; i < array.length; i++) {
//                try {
//                    JSONObject choise = (JSONObject) choices.get(i);
//                    array[i] = choise.getString("text");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);
//            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spinnerResults.setAdapter(spinnerArrayAdapter);
        } else {
            spinnerResults.setVisibility(View.INVISIBLE);
            buttonResults.setVisibility(View.INVISIBLE);
        }
    }
//                makeRequest(requestText, n, tokens, topProb, pres, freq, lProb);

    private void makeRequest(String text, String n, String tokens, String topProb, String pres, String freq, String lProb, String stop) {

       radioGroup.removeAllViews();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("max_tokens", Integer.valueOf(tokens));
            jsonObject.put("top_p", Float.valueOf(topProb));
            /*HERE IS OUR TEXTVIEW TEXT*/
            jsonObject.put("prompt", text);
            jsonObject.put("n", Integer.valueOf(n));
            jsonObject.put("stream", false);
            jsonObject.put("presence_penalty", Float.valueOf(pres));
            jsonObject.put("frequency_penalty", Float.valueOf(freq));
            if (lProb.equals("")) {
                jsonObject.put("logprobs", null);

            } else {
                jsonObject.put("logprobs", Integer.valueOf(lProb));

            }

            //TODO: search why stop is not working as an array.
/*
            List<String> listStrings = new ArrayList<String>();
            listStrings.add(".");
            jsonObject.put("stop", listStrings);
*/

//            jsonObject.put("stop", ".");
            jsonObject.put("stop", stop);

            Log.d("request body", String.valueOf(jsonObject));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RunTimeStorage.getInstace().setTextRequest(text);
        // Endpoint.getInstace().makePost(jsonObject, MainActivity.this, spinnerResults);
        String jsonex = jsonObject.toString();
        bearertoken = "Bearer "+ bearertoken;
        Endpoint.getInstace().sendPostRequest(Constants.ENGINES_DAVINCI_COMPLETIONS, MainActivity.this, jsonex, spinnerResults, radioGroup, bearertoken);
        //Endpoint.getInstace().sendToSlack(MainActivity.this, jsonObject);
    }

}
