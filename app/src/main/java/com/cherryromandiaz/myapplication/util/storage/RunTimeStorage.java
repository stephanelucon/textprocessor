package com.cherryromandiaz.myapplication.util.storage;

import org.json.JSONArray;

public class RunTimeStorage {

    private static final String TAG = "RunTimeStorage";
    private static RunTimeStorage runTimeStorage;

    public static RunTimeStorage getInstace() {
        if (runTimeStorage == null)
            runTimeStorage = new RunTimeStorage();
        return runTimeStorage;
    }

    JSONArray textArray;
    public void setTextArray(JSONArray textArray){
        this.textArray = textArray;
    }
    public JSONArray getTextArray(){
        return this.textArray;
    }

    String textRequest;
    public void setTextRequest (String textRequest){
        this.textRequest = textRequest;
    }
    public String getTextRequest(){
        return this.textRequest;
    }

}
