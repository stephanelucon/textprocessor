package com.cherryromandiaz.myapplication.util.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPreferencesManager {
    private static final String TAG = "SharedPrefMan";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sharedPreferencesEditor;

    private static String MODELIN = "MODELIN";
    private static String MODELOUT = "MODELOUT";

    private static String ORIGIN = "ORIGIN";
    private static String DESTINATION = "DESTINATION";

    @SuppressLint("LongLogTag")
    public static void initializePreferences(Context context) {
        //      Log.d(TAG,"INITIALIZE PREFERENCES");
        sharedPreferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    synchronized public static String getModelIn() {
        return sharedPreferences.getString(MODELIN, "");
    }

    synchronized public static void setModelIn(String modelin) {
        sharedPreferencesEditor.putString(MODELIN, modelin);
        sharedPreferencesEditor.apply();
    }

    synchronized public static String getModelOut() {
        return sharedPreferences.getString(MODELOUT, "");
    }

    synchronized public static void setModelOut(String modelout) {
        sharedPreferencesEditor.putString(MODELOUT, modelout);
        sharedPreferencesEditor.apply();
    }

    synchronized public static String getOrigin() {
        return sharedPreferences.getString(ORIGIN, "");
    }

    synchronized public static void setOrigin(String origin) {
        sharedPreferencesEditor.putString(ORIGIN, origin);
        sharedPreferencesEditor.apply();
    }

    synchronized public static String getDestination() {
        return sharedPreferences.getString(DESTINATION, "");
    }

    synchronized public static void setDestination(String destination) {
        sharedPreferencesEditor.putString(DESTINATION, destination);
        sharedPreferencesEditor.apply();
    }
}
