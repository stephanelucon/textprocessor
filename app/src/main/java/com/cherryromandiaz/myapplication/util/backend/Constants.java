package com.cherryromandiaz.myapplication.util.backend;

public class Constants {
    /**
     * URLS
     */
    public static final String ENGINES_DAVINCI_COMPLETIONS = "https://api.openai.com/v1/engines/davinci/completions";
}
