package com.cherryromandiaz.myapplication.util.backend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cherryromandiaz.myapplication.util.Logger;
import com.cherryromandiaz.myapplication.util.storage.RunTimeStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class Endpoint {

    private static final String TAG = "Endpoint";
    private static Endpoint endpoint;

    public static Endpoint getInstace() {
        if (endpoint == null)
            endpoint = new Endpoint();
        return endpoint;
    }

    private static void PostMessage(final String requestURL, final JSONObject request) {
        Log.d(TAG, "PostMessage");

        Thread executionThread = new Thread(new Runnable() {
            String response;

            @Override
            public void run() {
                try {
                    final URL url = new URL(requestURL);

                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);


                    DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                    dos.write(request.toString().getBytes());

                    dos.flush();
                    dos.close();

                    Log.i(TAG, String.valueOf(conn.getResponseCode()));
                    response = conn.getResponseMessage();
                    Log.i(TAG, response);
                    if (String.valueOf(conn.getResponseCode()).equals("200")) {
                        Log.d(TAG, "Message posted");
                    } else {
                        Log.d(TAG, "Message not posted");


                    }
                    conn.disconnect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        executionThread.start();
    }

    public void sendPostRequest(final String url, final Context context, final String jsonex, final Spinner spinner, final RadioGroup radioGroup, final String bearer) {

        @SuppressLint("StaticFieldLeak")
        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {
            @Override
            protected String doInBackground(Object... params) {
                String thUrl = (String) params[0];
                StringBuilder stringBuilder = new StringBuilder();
                String newString;
                newString = jsonex;
                try {
                    URL reqURL = new URL(thUrl);
                    HttpURLConnection request = (HttpURLConnection) (reqURL
                            .openConnection());
                    request.setDoOutput(true);
                    request.setRequestProperty("Content-Type",
                            "application/json");
                    request.setRequestProperty("Authorization", bearer); 
                    request.setConnectTimeout(10000);

                    request.setRequestMethod("POST");
                    try {
                        request.connect();
                    } catch (Exception e) {
                        Log.d(TAG, "connection error");
                    }
                    OutputStreamWriter writer = new OutputStreamWriter(
                            request.getOutputStream());
                    writer.write(newString);
                    writer.flush();
                    InputStream inputStream = request.getInputStream();
                    BufferedReader bufferedReader = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        bufferedReader = new BufferedReader(new InputStreamReader(
                                inputStream,
                                StandardCharsets.UTF_8), 8000);
                    }
                    String bufferedStrChunk;
                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Second Exception caz of HttpResponse :"
                            + ioe);
                    ioe.printStackTrace();
                }
                return stringBuilder.toString();
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, String.valueOf(response));
                try {
                    JSONObject reader = new JSONObject(response);
                    JSONArray choices = reader.getJSONArray("choices");
                    Logger.logText(choices);
                    RunTimeStorage.getInstace().setTextArray(choices);

                    String[] array = new String[RunTimeStorage.getInstace().getTextArray().length()];
                    for (int i = 0; i < array.length; i++) {
                        try {
                            JSONObject choise = (JSONObject) choices.get(i);
                            array[i] = choise.getString("text");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    String str = TextUtils.join("\n", array);
                    //Toast.makeText(context, str, Toast.LENGTH_LONG).show();
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, array);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);

                    RadioButton button;
                    for(int i = 0; i < RunTimeStorage.getInstace().getTextArray().length(); i++) {
                        try {
                            JSONObject choise = (JSONObject) choices.get(i);
                            String text = choise.getString("text");

                            button = new RadioButton(context);
                            button.setText(text);
                            radioGroup.addView(button);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(url, "post", jsonex);

        Toast.makeText(context, "=", Toast.LENGTH_SHORT).show();
    }

    public void sendToSlack(Context context, Object document) {

        document = "Request to openAI" + document;
        String URL = "https://hooks.slack.com/services/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";
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("text", document);
        } catch (JSONException JS) {
            Log.d(TAG, "JSON Exception : " + JS);
        }

        PostMessage(URL, jsonObject);
    }
}
