package com.cherryromandiaz.myapplication.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

public class Logger {
    public static final String TAG = "Logger";

    public static void logText(JSONArray choices) {
        try {
            for (int i = 0; i < choices.length(); i++) {
                JSONObject choise = (JSONObject) choices.get(i);
                Log.d(TAG, "Text is " + choise.getString("text"));
            }
        } catch (Exception ignored) {

        }
    }
}
